class CommonUtils {
  static isEmptyValidator(String value) {
    return value.isEmpty ? 'enter value' : null;
  }

  static isArrayValidAndNotEmpty(array) {
    return array.runtimeType.toString().startsWith('List') && array.length != 0;
  }
}
