import 'dart:developer';

import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  setString(String key, String value) async {
    log('sharedPrefWrite $key => $value');
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(key, value);
  }

  getString(String key) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.get(key);
  }

  setInt(String key, int value) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt(key, value);
  }

  getInt(String key) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.get(key);
  }

  setDouble(String key, String value) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(key, value);
  }

  getDouble(String key) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.get(key);
  }
}
