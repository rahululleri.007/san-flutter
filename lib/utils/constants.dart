import 'package:flutter/material.dart';
import 'package:study_all_night/data_models/department.dart';
import 'package:study_all_night/flutter_flow/flutter_flow_theme.dart';

List<Department> departments = [
  Department(deptId: 1, name: 'Computer Science', code: 'CSE'),
];

InputDecoration inputDecoration(BuildContext context, String hint) {
  return InputDecoration(
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(
        color: Color(0xFF5B60E2),
        width: 2,
      ),
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(4.0),
        topRight: Radius.circular(4.0),
      ),
    ),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(
        color: Color(0xFF5B60E2),
        width: 2,
      ),
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(4.0),
        topRight: Radius.circular(4.0),
      ),
    ),
    contentPadding: EdgeInsetsDirectional.fromSTEB(
      15,
      0,
      0,
      0,
    ),
    hintText: hint,
    hintStyle: FlutterFlowTheme.of(context).bodyText1.override(
          fontFamily: 'Poppins',
          color: Color(0xFF71717A),
          fontSize: 14,
        ),
  );
}
