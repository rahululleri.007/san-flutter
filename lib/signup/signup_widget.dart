// import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'dart:developer';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:study_all_night/api_handlers/user_api_handler.dart';
import 'package:study_all_night/data_models/department.dart';
import 'package:study_all_night/utils/common_util.dart';
import 'package:study_all_night/utils/constants.dart';

import '../flutter_flow/flutter_flow_icon_button.dart';
import '../flutter_flow/flutter_flow_theme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import '../login/login_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class SignupWidget extends StatefulWidget {
  const SignupWidget({Key key}) : super(key: key);

  @override
  _SignupWidgetState createState() => _SignupWidgetState();
}

class _SignupWidgetState extends State<SignupWidget> {
  TextEditingController nameController;
  TextEditingController idController;
  TextEditingController passwordController;
  TextEditingController emailController;
  TextEditingController deptController;
  Department department;
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  int yearOfStudy;
  @override
  void initState() {
    super.initState();
    nameController = TextEditingController();
    idController = TextEditingController();
    emailController = TextEditingController();
    deptController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: FlutterFlowTheme.of(context).primaryBackground,
      body: SafeArea(
        child: SingleChildScrollView(
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              children: [
                Hero(
                  tag: 'BG',
                  transitionOnUserGestures: true,
                  child: Image.asset(
                    'assets/images/Sign_Up.png',
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 1,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(25, 0, 25, 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/SAN.png',
                        width: 75,
                        height: 75,
                        fit: BoxFit.none,
                      ),
                      Divider(
                        height: 50,
                        color: Color(0xFFFAFAFA),
                      ),
                      Align(
                        alignment: AlignmentDirectional(-0.85, 0),
                        child: Text(
                          'Sign Up',
                          textAlign: TextAlign.start,
                          style: FlutterFlowTheme.of(context).title1.override(
                                fontFamily: 'Poppins',
                                color: Color(0xFF525257),
                                fontSize: 36,
                              ),
                        ),
                      ),
                      Divider(
                        height: 30,
                        color: Color(0xFFFAFAFA),
                      ),
                      Padding(
                        padding: EdgeInsetsDirectional.fromSTEB(15, 0, 15, 0),
                        child: Form(
                          key: formKey,
                          autovalidateMode: AutovalidateMode.disabled,
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              TextFormField(
                                controller: nameController,
                                validator: (value) =>
                                    CommonUtils.isEmptyValidator(value),
                                obscureText: false,
                                decoration: InputDecoration(
                                  hintText: 'Name',
                                  hintStyle: FlutterFlowTheme.of(context)
                                      .bodyText1
                                      .override(
                                        fontFamily: 'Poppins',
                                        color: Color(0xFF71717A),
                                        fontSize: 14,
                                      ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  contentPadding:
                                      EdgeInsetsDirectional.fromSTEB(
                                          15, 0, 0, 0),
                                ),
                                style: FlutterFlowTheme.of(context)
                                    .bodyText1
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF5B60E2),
                                      fontSize: 14,
                                    ),
                              ),
                              Divider(
                                height: 25,
                              ),
                              TextFormField(
                                controller: idController,
                                obscureText: false,
                                decoration: InputDecoration(
                                  hintText: 'University ID',
                                  hintStyle: FlutterFlowTheme.of(context)
                                      .bodyText1
                                      .override(
                                        fontFamily: 'Poppins',
                                        color: Color(0xFF71717A),
                                        fontSize: 14,
                                      ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  contentPadding:
                                      EdgeInsetsDirectional.fromSTEB(
                                          15, 0, 0, 0),
                                ),
                                style: FlutterFlowTheme.of(context)
                                    .bodyText1
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF5B60E2),
                                      fontSize: 14,
                                    ),
                              ),
                              Divider(
                                height: 25,
                              ),
                              TextFormField(
                                controller: emailController,
                                obscureText: false,
                                decoration: InputDecoration(
                                  hintText: 'Email',
                                  hintStyle: FlutterFlowTheme.of(context)
                                      .bodyText1
                                      .override(
                                        fontFamily: 'Poppins',
                                        color: Color(0xFF71717A),
                                        fontSize: 14,
                                      ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  contentPadding:
                                      EdgeInsetsDirectional.fromSTEB(
                                          15, 0, 0, 0),
                                ),
                                style: FlutterFlowTheme.of(context)
                                    .bodyText1
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF5B60E2),
                                      fontSize: 14,
                                    ),
                              ),
                              Divider(
                                height: 25,
                              ),
                              DropdownButtonFormField<Department>(
                                items: departments
                                    .map((e) => DropdownMenuItem(
                                          value: e,
                                          child: Text(
                                            e.name,
                                          ),
                                        ))
                                    .toList(),
                                onChanged: ((value) {
                                  department = value;
                                }),
                                value: department,
                                icon: const Icon(Icons.arrow_downward),
                                elevation: 16,
                                style:
                                    const TextStyle(color: Colors.deepPurple),
                                // underline: Container(
                                //   height: 2,
                                //   color: Colors.deepPurpleAccent,
                                // ),
                                decoration:
                                    inputDecoration(context, 'Department'),
                              ),
                              Divider(
                                height: 25,
                              ),
                              DropdownButtonFormField(
                                items: [1, 2, 3, 4]
                                    .map((e) => DropdownMenuItem(
                                          value: e,
                                          child: Text(
                                            e.toString(),
                                          ),
                                        ))
                                    .toList(),
                                onChanged: ((value) {
                                  yearOfStudy = value;
                                }),
                                value: yearOfStudy,
                                icon: const Icon(Icons.arrow_downward),
                                elevation: 16,
                                style:
                                    const TextStyle(color: Colors.deepPurple),
                                // underline: Container(
                                //   height: 2,
                                //   color: Colors.deepPurpleAccent,
                                // ),
                                decoration: InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  contentPadding:
                                      EdgeInsetsDirectional.fromSTEB(
                                    15,
                                    0,
                                    0,
                                    0,
                                  ),
                                  hintText: 'Year Of study',
                                  hintStyle: FlutterFlowTheme.of(context)
                                      .bodyText1
                                      .override(
                                        fontFamily: 'Poppins',
                                        color: Color(0xFF71717A),
                                        fontSize: 14,
                                      ),
                                ),
                              ),
                              Divider(
                                height: 25,
                              ),
                              TextFormField(
                                controller: passwordController,
                                obscureText: false,
                                decoration: InputDecoration(
                                  hintText: 'Password',
                                  hintStyle: FlutterFlowTheme.of(context)
                                      .bodyText1
                                      .override(
                                        fontFamily: 'Poppins',
                                        color: Color(0xFF71717A),
                                        fontSize: 14,
                                      ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFF5B60E2),
                                      width: 2,
                                    ),
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                  ),
                                  contentPadding:
                                      EdgeInsetsDirectional.fromSTEB(
                                          15, 0, 0, 0),
                                ),
                                style: FlutterFlowTheme.of(context)
                                    .bodyText1
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF5B60E2),
                                      fontSize: 14,
                                    ),
                              ),
                              Divider(
                                height: 50,
                              ),
                              FlutterFlowIconButton(
                                borderColor: Colors.transparent,
                                borderRadius: 30,
                                borderWidth: 1,
                                buttonSize: 60,
                                fillColor: Color(0xFF5B60E2),
                                icon: FaIcon(
                                  FontAwesomeIcons.arrowRight,
                                  color: Color(0xFFFAFAFA),
                                  size: 20,
                                ),
                                onPressed: () async {
                                  _createUser(context);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _createUser(BuildContext context) async {
    final userData = {
      'name': nameController.text,
      'stud_id': idController.text,
      'university': 'KTU',
      'year': yearOfStudy.toString(),
      'email': emailController.text,
      'dept_id': department?.deptId.toString(),
      'password': passwordController.text,
    };
    log('userData $userData');
    final UserApiHandler handler = UserApiHandler();
    final res = await handler.createUser(userData);
    log('res $res');

    if (res != null) {
      await Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: ((context) => LoginWidget())),
          (route) => false);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('SignUp Failed'),
      ));
    }
  }

  Future<List<Department>> _fetchDept(String pattern) async {
    final UserApiHandler _handler = UserApiHandler();
    final data = await _handler.fetchDept(pattern);
    // return _handler.fetchDept(pattern);
    log('data ${data}');
    return data;
  }
}
