import '../flutter_flow/flutter_flow_theme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import '../flutter_flow/flutter_flow_widgets.dart';
import '../home/home_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class YearselectWidget extends StatefulWidget {
  const YearselectWidget({Key key}) : super(key: key);

  @override
  _YearselectWidgetState createState() => _YearselectWidgetState();
}

class _YearselectWidgetState extends State<YearselectWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFAFAFA),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Hero(
                tag: 'BG',
                transitionOnUserGestures: true,
                child: Image.asset(
                  'assets/images/Semester_Selection.png',
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 1,
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(25, 0, 25, 0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/SAN.png',
                      width: 75,
                      height: 75,
                      fit: BoxFit.none,
                    ),
                    Divider(
                      height: 50,
                      color: Color(0xFFFAFAFA),
                    ),
                    Align(
                      alignment: AlignmentDirectional(-0.85, 0),
                      child: Text(
                        'Year\nOf\nStudy?',
                        textAlign: TextAlign.start,
                        style: FlutterFlowTheme.of(context).title1.override(
                              fontFamily: 'Poppins',
                              color: Color(0xFF525257),
                              fontSize: 36,
                            ),
                      ),
                    ),
                    Divider(
                      height: 50,
                      color: Color(0xFFFAFAFA),
                    ),
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(15, 0, 15, 0),
                      child: InkWell(
                        onTap: () async {
                          await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => HomeWidget(),
                            ),
                          );
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            FFButtonWidget(
                              onPressed: () {
                                print('Button pressed ...');
                              },
                              text: '1',
                              options: FFButtonOptions(
                                width: 56,
                                height: 56,
                                color: Color(0xFFFAFAFA),
                                textStyle: FlutterFlowTheme.of(context)
                                    .subtitle2
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF5B60E2),
                                      fontSize: 24,
                                      fontWeight: FontWeight.normal,
                                    ),
                                borderSide: BorderSide(
                                  color: Color(0xFF5B60E2),
                                  width: 3,
                                ),
                                borderRadius: 50,
                              ),
                            ),
                            FFButtonWidget(
                              onPressed: () {
                                print('Button pressed ...');
                              },
                              text: '2\n',
                              options: FFButtonOptions(
                                width: 56,
                                height: 56,
                                color: Color(0xFFFAFAFA),
                                textStyle: FlutterFlowTheme.of(context)
                                    .subtitle2
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF5B60E2),
                                      fontSize: 24,
                                      fontWeight: FontWeight.normal,
                                    ),
                                borderSide: BorderSide(
                                  color: Color(0xFF5B60E2),
                                  width: 3,
                                ),
                                borderRadius: 50,
                              ),
                            ),
                            FFButtonWidget(
                              onPressed: () {
                                print('Button pressed ...');
                              },
                              text: '3\n',
                              options: FFButtonOptions(
                                width: 56,
                                height: 56,
                                color: Color(0xFFFAFAFA),
                                textStyle: FlutterFlowTheme.of(context)
                                    .subtitle2
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF5B60E2),
                                      fontSize: 24,
                                      fontWeight: FontWeight.normal,
                                    ),
                                borderSide: BorderSide(
                                  color: Color(0xFF5B60E2),
                                  width: 3,
                                ),
                                borderRadius: 50,
                              ),
                            ),
                            FFButtonWidget(
                              onPressed: () {
                                print('Button pressed ...');
                              },
                              text: '4',
                              options: FFButtonOptions(
                                width: 56,
                                height: 56,
                                color: Color(0xFFFAFAFA),
                                textStyle: FlutterFlowTheme.of(context)
                                    .subtitle2
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF5B60E2),
                                      fontSize: 24,
                                      fontWeight: FontWeight.normal,
                                    ),
                                borderSide: BorderSide(
                                  color: Color(0xFF5B60E2),
                                  width: 3,
                                ),
                                borderRadius: 50,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
