import 'package:study_all_night/data_models/department.dart';

class Subject {
  int subjectId;
  Department deptId;
  String name;
  String universityId;
  int year;
  bool voided;

  Subject(
      {this.subjectId,
      this.deptId,
      this.name,
      this.universityId,
      this.year,
      this.voided});

  Subject.fromJson(Map<String, dynamic> json) {
    subjectId = json['subject_id'];
    deptId = json['dept_id'] != null
        ? new Department.fromJson(json['dept_id'])
        : null;
    name = json['name'];
    universityId = json['university_id'];
    year = json['year'];
    voided = json['voided'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subject_id'] = this.subjectId;
    if (this.deptId != null) {
      data['dept_id'] = this.deptId.toJson();
    }
    data['name'] = this.name;
    data['university_id'] = this.universityId;
    data['year'] = this.year;
    data['voided'] = this.voided;
    return data;
  }
}
