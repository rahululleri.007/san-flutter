class Department {
  int deptId;
  String name;
  String code;
  bool voided;

  Department({this.deptId, this.name, this.code, this.voided});

  Department.fromJson(Map<String, dynamic> json) {
    deptId = json['dept_id'];
    name = json['name'];
    code = json['code'];
    voided = json['voided'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dept_id'] = this.deptId;
    data['name'] = this.name;
    data['code'] = this.code;
    data['voided'] = this.voided;
    return data;
  }
}
