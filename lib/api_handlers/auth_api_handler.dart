import 'dart:developer';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:study_all_night/utils/shared_pref.dart';
import 'package:http/http.dart' as http;
import 'api_path.dart' as API_PATH;

class AuthApiHandler {
  final SharedPref _sharedPref = SharedPref();
  final FlutterSecureStorage _secureStorage = FlutterSecureStorage();

  Future getToken() async {
    final token = await _secureStorage.read(key: 'token');
    return token;
  }

  Future authenticateUser(String username, String password) async {
    try {
      final res = await http.post(
        Uri.parse(API_PATH.login),
        body: {
          "username": username,
          "password": password,
        },
      );
      log('authReps: ${res.body}  ${res.statusCode}');
      if (res.statusCode == 200) {
        await _sharedPref.setString('id', username);
        _secureStorage.write(key: 'id', value: username);
        return res.body;
      }
    } catch (e) {
      log('errorOnLogin : ' + e.toString());
    }
    return null;
  }
}
