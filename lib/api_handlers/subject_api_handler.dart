import 'dart:convert';
import 'dart:developer';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:study_all_night/data_models/subject.dart';
import 'package:study_all_night/utils/shared_pref.dart';
import 'package:http/http.dart' as http;
import 'api_path.dart' as API_PATH;

class SubjectApiHandler {
  final SharedPref _sharedPref = SharedPref();
  final FlutterSecureStorage _secureStorage = FlutterSecureStorage();

  Future getSubjectsForStudent() async {
    final id = await _sharedPref.getString('id');
    final secureId = await _secureStorage.read(key: 'id');
    log('getSubjectsForStudentId $id $secureId');
    List<Subject> subjects = [];
    try {
      final res = await http.get(
        Uri.parse('${API_PATH.subjectForStudents}$secureId'),
      );
      log('authReps: ${res.body}  ${res.statusCode}');
      if (res.statusCode == 200) {
        final _data = json.decode(res.body);
        if (_data != null) {
          _data.forEach((element) {
            if (element['voided'] == false)
              subjects.add(Subject.fromJson(element));
          });
        }
      }
    } catch (e) {
      log('errorOnLogin : ' + e.toString());
    }
    return subjects;
  }
}
