import 'dart:convert';
import 'dart:developer';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:study_all_night/data_models/department.dart';
import 'package:study_all_night/utils/shared_pref.dart';
import 'package:http/http.dart' as http;
import 'api_path.dart' as API_PATH;

class UserApiHandler {
  final SharedPref _sharedPref = SharedPref();
  final FlutterSecureStorage _secureStorage = FlutterSecureStorage();

  Future getToken() async {
    final token = await _secureStorage.read(key: 'token');
    return token;
  }

  Future createUser(userData) async {
    final token = await _secureStorage.read(key: 'token');
    try {
      final res = await http.post(
        Uri.parse(API_PATH.signUp),
        body: userData,
      );
      log('authReps: ${res.body}  ${res.statusCode}');
      if (res.statusCode == 200) {
        await _secureStorage.write(key: 'id', value: userData['stud_id']);
        return res.body;
      }
    } catch (e) {
      log('errorOnLogin : ' + e.toString());
    }
    return null;
  }

  Future<List<Department>> fetchDept(String pattern) async {
    List<Department> departments = [];

    try {
      final res = await http
          .post(Uri.parse(API_PATH.searchDept), body: {'str': pattern});
      final _data = json.decode(res.body);
      log('fetchDept ${pattern} ${_data} ======= ${res.body}');
      if (_data != null) {
        _data.forEach((element) {
          if (element['voided'] == false)
            departments.add(Department.fromJson(element));
        });
      }
    } catch (e) {}
    return departments;
  }

  Future getUserData() {}
}
