import 'package:study_all_night/api_handlers/auth_api_handler.dart';
import 'package:study_all_night/index.dart';

import '../flutter_flow/flutter_flow_icon_button.dart';
import '../flutter_flow/flutter_flow_theme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import '../signup/signup_widget.dart';
import '../yearselect/yearselect_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginWidget extends StatefulWidget {
  const LoginWidget({Key key}) : super(key: key);

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  TextEditingController usernameController =
      TextEditingController(text: '1002');
  TextEditingController passwordController =
      TextEditingController(text: 'password');
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  bool isLoggingIn = false;
  @override
  void initState() {
    super.initState();
    // usernameController = ;
    // passwordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFAFAFA),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Hero(
                tag: 'BG',
                transitionOnUserGestures: true,
                child: Image.asset(
                  'assets/images/Sign_Up.png',
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 1,
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(25, 0, 25, 0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/SAN.png',
                      width: 75,
                      height: 75,
                      fit: BoxFit.none,
                    ),
                    Divider(
                      height: 50,
                      color: Color(0xFFFAFAFA),
                    ),
                    Align(
                      alignment: AlignmentDirectional(-0.85, 0),
                      child: Text(
                        'Log In',
                        textAlign: TextAlign.start,
                        style: FlutterFlowTheme.of(context).title1.override(
                              fontFamily: 'Poppins',
                              color: Color(0xFF525257),
                              fontSize: 36,
                            ),
                      ),
                    ),
                    Divider(
                      height: 35,
                      color: Color(0xFFFAFAFA),
                    ),
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(15, 0, 15, 0),
                      child: Form(
                        key: formKey,
                        autovalidateMode: AutovalidateMode.disabled,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextFormField(
                              controller: usernameController,
                              obscureText: false,
                              decoration: InputDecoration(
                                hintText: 'USername',
                                hintStyle: FlutterFlowTheme.of(context)
                                    .bodyText1
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF71717A),
                                      fontSize: 14,
                                    ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xFF5B60E2),
                                    width: 2,
                                  ),
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xFF5B60E2),
                                    width: 2,
                                  ),
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                contentPadding:
                                    EdgeInsetsDirectional.fromSTEB(15, 0, 0, 0),
                              ),
                              style: FlutterFlowTheme.of(context)
                                  .bodyText1
                                  .override(
                                    fontFamily: 'Poppins',
                                    color: Color(0xFF5B60E2),
                                    fontSize: 14,
                                  ),
                            ),
                            Divider(
                              height: 25,
                            ),
                            TextFormField(
                              controller: passwordController,
                              obscureText: false,
                              decoration: InputDecoration(
                                hintText: 'Password',
                                hintStyle: FlutterFlowTheme.of(context)
                                    .bodyText1
                                    .override(
                                      fontFamily: 'Poppins',
                                      color: Color(0xFF71717A),
                                      fontSize: 14,
                                    ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xFF5B60E2),
                                    width: 2,
                                  ),
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xFF5B60E2),
                                    width: 2,
                                  ),
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                contentPadding:
                                    EdgeInsetsDirectional.fromSTEB(15, 0, 0, 0),
                              ),
                              style: FlutterFlowTheme.of(context)
                                  .bodyText1
                                  .override(
                                    fontFamily: 'Poppins',
                                    color: Color(0xFF5B60E2),
                                    fontSize: 14,
                                  ),
                            ),
                            Divider(
                              height: 65,
                            ),
                            // TODO replace this button with another one
                            FlutterFlowIconButton(
                              borderColor: Colors.transparent,
                              borderRadius: 30,
                              borderWidth: 1,
                              buttonSize: 60,
                              fillColor: Color(0xFF5B60E2),
                              icon: FaIcon(
                                isLoggingIn
                                    ? FontAwesomeIcons.circle
                                    : FontAwesomeIcons.arrowRight,
                                color: Color(0xFFFAFAFA),
                                size: 20,
                              ),
                              onPressed: () async {
                                isLoggingIn ? () => {} : _loginUser(context);
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    Divider(
                      height: 200,
                      color: Color(0xFFFAFAFA),
                    ),
                    InkWell(
                      onTap: () async {
                        // _loginUser(context);
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SignupWidget(),
                          ),
                        );
                      },
                      child: Text(
                        'Create New Account',
                        style: FlutterFlowTheme.of(context).bodyText1.override(
                              fontFamily: 'Poppins',
                              color: Color(0xFFFAFAFA),
                              decoration: TextDecoration.underline,
                            ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _loginUser(BuildContext context) async {
    setState(() {
      isLoggingIn = true;
    });

    final AuthApiHandler _apiHandler = AuthApiHandler();
    final res = await _apiHandler.authenticateUser(
        usernameController.text, passwordController.text);
    setState(() {
      isLoggingIn = false;
    });
    if (res != null) {
      await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => HomeWidget(),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Login Failed'),
      ));
    }
  }
}
